import random

slowa = {
    "python": ["pierwsza litera to p", "jest to język programowania", "piszesz w tym"],
    "gdansk": ["pierwsza litera to g", "jest to miasto", "Jest w trójmiescie"],
    "dlaczego": ["pierwsza litera to d", "czesto zaczynasz tym pytaniem", "ostania litera to o"],
    "gdynia": ["pierwsza litera to g", "jest to miasto", "Jest w trójmiescie"],
    "wsb": ["pierwsza litera to w", "nazwa szkoły", "uczysz się tam"]}
proba = 0
punktacja = {
    0: 5,
    1: 4,
    2: 3,
    3: 2,
    4: 1,
    5: 0
}
word = random.choice(list(slowa))
# print (word)
poprawnie = word
pomie = ""
while word:
    position = random.randrange(len(word))
    pomie += word[position]
    word = word[:position] + word[(position + 1):]
print(pomie)

print(
    """
    Witaj w grze 'Wymieszane litery'!  
    Uporządkuj litery, aby odtworzyć prawidłowe słowo.
    (Aby zakończyć zgadywanie, naciśnij klawisz Enter bez podawania odpowiedzi.)
    """
)
print("Zgadnij, jakie to słowo:", pomie)
zgaduj = input("Twoja odpowiedź: ")
while zgaduj != poprawnie and zgaduj != "":
    print("Niestety, to nie to słowo.")
    proba += 1
    podpowiedz = random.choice(slowa[poprawnie])
    print(podpowiedz)
    zgaduj = input("Twoja odpowiedź: ")
if zgaduj == poprawnie:
    print("Zgadza się! Zgadłeś! \n")
    iloscPunktow = punktacja.get(proba)
    print("Ilosc punktów: ", iloscPunktow)
print("Dziękuję za udział w grze.")
