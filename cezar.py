def szyfruj(zdanie, klucz):
    wynik = ''

    for litera in zdanie:
        liczbaJakoAscii = ord(litera)
        if 64 < liczbaJakoAscii < 91 or 96 < liczbaJakoAscii < 123:
            liczbaJakoAscii = liczbaJakoAscii + klucz
        if 90 < liczbaJakoAscii < 97 or liczbaJakoAscii > 122:
            liczbaJakoAscii -= 26
        wynik += chr(liczbaJakoAscii)
    return wynik
