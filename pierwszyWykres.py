import numpy as np
import matplotlib.pyplot as plt

X = np.linspace(-np.pi, np.pi, 256, endpoint=True)

C, S = np.cos(X), np.sin(X)
plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-")
plt.plot(X, S, color="green", linewidth=2.0, linestyle="-")

x = np.arange(0.0, 2.0, 0.01)
y = np.sin(2.0 * np.pi * x)
plt.plot(x, y, ':r', linewidth=6)
plt.xlabel('Czas')
plt.ylabel('Pozycja')
plt.title('Nasz pierwszy wykres')
plt.grid(True)

# plt.figure(figsize=(8, 6), dpi=80)
# plt.subplot(1, 1, 1)
# X=np.linspace(-np.pi, np.pi, 256, endpoint=True)
# C, S=np.cos(X), np.sin(X)

plt.show()
