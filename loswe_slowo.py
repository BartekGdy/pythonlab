import random

slowa = ("python", "gdansk", "dlaczego", "gdynia")
proba = 0
word = random.choice(list(slowa))
print(
    """
    Witaj w grze 'Zgadnij słowo'!  
    Podawaj litery aby ułatwić wygraną
    (Aby zakończyć zgadywanie, naciśnij klawisz Enter bez podawania odpowiedzi.)
    """
)
while proba < 5:
    zgaduj = input("Podaj literę: ")
    proba += 1
    if zgaduj in word:
        print("Tak")
    else:
        print("Nie")
odpowiedz = input("Podaj slowo: ")

if odpowiedz == word:
    print("Zgadza się! Zgadłeś! \n")
else:
    print("Przykro mi nie zgadłeś, słowo to: ", word)
print("Dziękuję za udział w grze.")
