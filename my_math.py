def srednia(oceny):
    sum = 0
    for ocena in oceny:
        sum += int(ocena)
    return sum / len(oceny)


def mediana(oceny):
    oceny = sorted(oceny)
    length = len(oceny)
    midleIndex = int(length / 2)
    if length % 2 == 0:
        return (int(oceny[midleIndex - 1]) + int(oceny[midleIndex])) / 2
    else:
        return oceny[midleIndex]


def odchylenie_st(oceny):
    sredniaOcen = srednia(oceny)
    length = len(oceny)
    wariancja = 0
    for ocena in oceny:
        wariancja += pow(int(ocena) - sredniaOcen, 2)
    wariancja /= length
    return pow(pow(wariancja, 2), 1 / 2)
