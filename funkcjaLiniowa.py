import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-10, 10, 1)
a = int(input('Podaj a '))
b = int(input('Podaj b '))

y = x * a + b

monotonicznosc = ''
if a > 0:
    monotonicznosc = 'Funkcja rosnąca'
elif a < 0:
    monotonicznosc = 'Funkcja malejąca'
else:
    monotonicznosc = 'Funkcja stała'

plt.plot(x, y, color="blue", label='Funkcja liniowa')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Wykres f(x) = ax + b')
plt.annotate(monotonicznosc, xy=(5, 10))
plt.legend(loc='upper left')
plt.grid(True)

ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

plt.savefig("funkcjaLiniowa.pdf")

plt.show()
